FROM debian:10

ENV DOCKER_VERSION=20.10.14

RUN apt update && apt install -y curl
RUN curl -fsSL https://get.docker.com -o get-docker.sh && sh ./get-docker.sh

CMD ["/usr/bin/dockerd"]

